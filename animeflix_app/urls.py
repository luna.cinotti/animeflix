from django.urls import path
from django.views.generic import RedirectView

from .views import HomePageView, PremiumPageView, ChangeAccountToPremium, VideoPageView, AnimeDetailView, \
    VideoPageViewPremium, VideoPageViewAlphabetical, VideoPageViewSimulcast, MyAnimeListView, LoadMoreDataView, \
    VideoPageViewCategory, VideoPageViewPopular, VideoPageViewGenre, VideoPageViewSeason, \
    NewAnimeReview, UpdateAnimeReview, DeleteAnimeReview

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('video', RedirectView.as_view(url='video/updated'), name='video'),
    path('video/updated', VideoPageView.as_view(), name='v_updated'),
    path('video/simulcast', VideoPageViewSimulcast.as_view(), name='v_simulcast'),
    path('video/popular', VideoPageViewPopular.as_view(), name='v_popular'),
    path('video/category', VideoPageViewCategory.as_view(), name='v_category'),
    path('video/genre', VideoPageViewGenre.as_view(), name='v_genre'),
    path('video/season', VideoPageViewSeason.as_view(), name='v_season'),
    path('video/alphabetical', VideoPageViewAlphabetical.as_view(), name='v_alphabetical'),
    path('video/premium', VideoPageViewPremium.as_view(), name='v_premium'),
    path('video/anime', AnimeDetailView.as_view(), name='v_anime_detail'),
    path('video/anime/reviews', AnimeDetailView.as_view(), name='v_anime_reviews'),
    path('video/anime/reviews/new-review', NewAnimeReview.as_view(), name='v_anime_new_review'),
    path('video/anime/reviews/edit-review<int:id_review>', UpdateAnimeReview.as_view(), name='v_anime_edit_review'),
    path('video/anime/reviews/delete-review<pk>', DeleteAnimeReview.as_view(), name='v_anime_delete_review'),
    path('myAnimeList', MyAnimeListView.as_view(), name='myAnimeList'),
    path('loadMoreAnimes', LoadMoreDataView.as_view(), name='load_more'),
    path('premium', PremiumPageView.as_view(), name='premium'),
    path('premium/subscribed', ChangeAccountToPremium.as_view(), name='subscribe')
]
