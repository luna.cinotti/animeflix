from django import forms
from animeflix_app.models import Evaluation


class ReviewForm(forms.ModelForm):
    vote = forms.IntegerField(
        error_messages={
            'required': "Don't forget to assign a vote"
        },
        required=True,
        label="Vote*",
        widget=forms.NumberInput(attrs={"class": "d-none"})
    )

    title = forms.CharField(
        error_messages={
            'required': "Don't forget to assign a title"
        },
        required=True,
        label="Title*",
        widget=forms.TextInput(attrs={"class": "form-control form-input"})
    )

    review = forms.CharField(
        error_messages={
            'required': "Don't forget to write a review"
        },
        required=True,
        label="Review*",
        widget=forms.Textarea(attrs={"class": "form-control form-input", "autofocus": True})
    )

    class Meta:
        model = Evaluation
        fields = ['vote', 'title', 'review']

