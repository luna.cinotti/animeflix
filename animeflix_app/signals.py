from django.db.models.aggregates import Avg
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from animeflix_app.models import Evaluation


@receiver([post_save, post_delete], sender=Evaluation)
def update_anime_avg_rating(sender, instance, **kwargs):
    # Update creator thread and post counter
    anime = instance.anime
    anime.avg_rating = anime.evaluation_set.aggregate(Avg('vote'))['vote__avg']
    anime.save(update_fields=['avg_rating'])
