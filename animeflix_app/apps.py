from django.apps import AppConfig


class AnimeflixAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'animeflix_app'

    def ready(self):
        import animeflix_app.signals
