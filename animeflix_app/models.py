# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.

import datetime

from django.contrib.auth.models import User
from django.core.cache import cache
from django.db import models
from django.utils.timezone import is_aware, is_naive

from animeflix_project import settings


class Utente(models.Model):
    account = models.OneToOneField(User, on_delete=models.CASCADE, db_column='account', primary_key=True)
    eta = models.IntegerField(blank=True, null=True)
    nazionalita = models.CharField(max_length=50, blank=True, default='')
    abbonato = models.BooleanField(default=False)
    avatar = models.ImageField(upload_to='avatar', default='avatar/avatar-default.png')

    class Meta:
        db_table = 'ANIMEFLIX_utente'

    def __str__(self):
        if not self.account.is_active:
            return "user deleted"
        return self.account.get_username()

    def get_default_avatar_path(self):
        upload_folder = self.avatar.field.upload_to
        default_name = upload_folder + '-default.png'
        default_path = upload_folder + '/' + default_name
        return default_path

    def get_account_lists(self):
        return Lista.objects.filter(id_account=self).all()

    def last_seen(self):
        return cache.get('seen_%s' % self.account.username)

    def is_online(self):
        last_seen = self.last_seen()
        if last_seen:
            now = datetime.datetime.now()
            if now > last_seen + datetime.timedelta(seconds=settings.USER_ONLINE_TIMEOUT):
                return False
            else:
                return True
        else:
            return False

    def delete(self, using=None, keep_parents=False):
        if 'default' not in self.avatar.name:
            self.avatar.storage.delete(self.avatar.name)
        return super().delete(using, keep_parents)


class Categoria(models.Model):
    descrizione = models.CharField(primary_key=True, max_length=30)

    class Meta:
        db_table = 'ANIMEFLIX_categoria'


class Genere(models.Model):
    descrizione = models.CharField(primary_key=True, max_length=20)

    class Meta:
        db_table = 'ANIMEFLIX_genere'


class Anime(models.Model):
    categoria = models.ForeignKey(Categoria, on_delete=models.DO_NOTHING, db_column='categoria')
    titolo = models.CharField(max_length=50)
    trama = models.TextField()
    copertina = models.TextField()
    stato = models.BooleanField()
    premium = models.BooleanField()
    n_serie = models.IntegerField(blank=True, null=True, default=0)
    tot_episodi = models.IntegerField(blank=True, null=True, default=0)
    avg_rating = models.FloatField(default=0)

    class Meta:
        db_table = 'ANIMEFLIX_anime'
        ordering = ['titolo']

    def __str__(self):
        return f'{self.titolo}_{self.id}'

    def update_serie_counter(self):
        self.n_serie = self.serie_set.count()
        self.save(update_fields=['n_serie'])

    @property
    def short_trama(self):
        return self.trama if len(self.trama) < 35 else (self.trama[:33] + '..')

    def get_series(self):
        return Serie.objects.select_related('id_anime').filter(id_anime=self.id).order_by('id')

    def last_ep(self):
        return Episodio.objects.select_related('id_anime').filter(id_anime=self.id).order_by('-data_car',
                                                                                             '-ora_car').first()


class Animegenere(models.Model):
    id_anime = models.ForeignKey(Anime, on_delete=models.DO_NOTHING, db_column='id_anime')
    id_genere = models.ForeignKey(Genere, on_delete=models.DO_NOTHING, db_column='id_genere')

    class Meta:
        db_table = 'ANIMEFLIX_anime_genere'
        unique_together = [['id_anime', 'id_genere']]


class Stagionedellanno(models.Model):
    stagione = models.CharField(max_length=10)
    anno = models.IntegerField()

    class Meta:
        db_table = 'ANIMEFLIX_stagionedellanno'
        unique_together = [['stagione', 'anno']]

    def __str__(self):
        return f'{self.stagione} {self.anno}'


class Serie(models.Model):
    numero = models.FloatField()
    id_anime = models.ForeignKey(Anime, on_delete=models.CASCADE, db_column='id_anime')
    id_stag_anno = models.ForeignKey(Stagionedellanno, on_delete=models.DO_NOTHING, db_column='id_stag_anno')
    nome = models.CharField(max_length=50)
    n_episodi = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'ANIMEFLIX_serie'
        unique_together = [['id', 'id_anime']]

    def __str__(self):
        return f'{self.nome}_{self.id}'

    def update_n_episodi(self):
        self.n_episodi = self.episodio_set.count()
        self.save(update_fields=['n_episodi'])

    def get_episodes(self):
        return Episodio.objects.select_related('id_serie').filter(id_serie=self.id)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.id_anime.update_serie_counter()
        super().save()


class Episodio(models.Model):
    numero = models.FloatField()
    id_serie = models.ForeignKey(Serie, on_delete=models.CASCADE, db_column='id_serie')
    id_anime = models.ForeignKey(Anime, on_delete=models.CASCADE, db_column='id_anime')
    titolo = models.CharField(max_length=100)
    url_video = models.TextField()
    data_car = models.DateField(blank=True, null=True)
    ora_car = models.TimeField(blank=True, null=True)

    class Meta:
        db_table = 'ANIMEFLIX_episodio'
        unique_together = [['id', 'id_serie', 'id_anime']]

    def __str__(self):
        return f'{self.numero}_{self.titolo}_{self.id}'

    def daysPassed(self):
        delta = datetime.date.today() - self.data_car
        return delta.days

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.id_serie.update_n_episodi()
        super().save()


class Lista(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=12, null=False)
    id_account = models.ForeignKey(Utente, on_delete=models.CASCADE, to_field='account', db_column='id_account')

    class Meta:
        db_table = 'ANIMEFLIX_lista'
        unique_together = [['nome', 'id_account']]

    def get_animes(self):
        return ListaIncludeAnime.objects.filter(id_account=self.id_account, nome_lista=self).all()


class ListaIncludeAnime(models.Model):
    id_anime = models.ForeignKey(Anime, on_delete=models.CASCADE, db_column='id_anime')
    nome_lista = models.ForeignKey(Lista, on_delete=models.CASCADE, db_column='nome_lista', blank=True, null=True)
    id_account = models.ForeignKey(Utente, on_delete=models.CASCADE, db_column='id_account')

    class Meta:
        db_table = 'ANIMEFLIX_lista_include_anime'
        unique_together = [['id_anime', 'nome_lista', 'id_account']]


class Premium(models.Model):
    id = models.IntegerField(primary_key=True)
    durata = models.IntegerField(null=False)

    class Meta:
        db_table = 'ANIMEFLIX_premium'

    def __str__(self):
        return self.durata.__str__()


class AccountPremium(models.Model):
    id_account = models.ForeignKey(Utente, on_delete=models.CASCADE, db_column='id_account')
    id_premium = models.ForeignKey(Premium, on_delete=models.CASCADE, db_column='id_premium')
    data_inizio = models.DateField(auto_now_add=True)
    data_fine = models.DateField(editable=False)
    scaduto = models.BooleanField(default=False, editable=False)

    class Meta:
        db_table = 'ANIMEFLIX_account_premium'
        unique_together = [['id_account', 'id_premium']]

    def save(self, *args, **kwargs):
        offert = self.id_premium
        self.data_inizio = datetime.date.today()
        self.data_fine = self.data_inizio + datetime.timedelta(days=offert.durata)
        return super().save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.id_account.abbonato = False
        self.id_account.save(update_fields=['abbonato'])
        return super().delete(*args, **kwargs)


class Evaluation(models.Model):
    author = models.ForeignKey(Utente, on_delete=models.CASCADE, db_column='author')
    anime = models.ForeignKey(Anime, on_delete=models.CASCADE, db_column='anime')
    title = models.CharField(blank=False, max_length=100)
    review = models.TextField(blank=False)
    vote = models.PositiveSmallIntegerField(null=False)
    creation_date = models.DateTimeField(auto_now_add=True, editable=False)
    mod_date = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'ANIMEFLIX_evaluation'
        unique_together = [['author', 'anime']]
        ordering = ['creation_date']

    def creation_date_str(self):
        return self.creation_date.strftime("%d-%m-%Y")

    def mod_date_str(self):
        return self.mod_date.strftime("%d-%m-%Y")
