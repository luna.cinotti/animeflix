from django.contrib import admin
from .models import Anime, Animegenere, Genere, Serie, Stagionedellanno, Episodio, AccountPremium


@admin.register(Anime)
class AnimeAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'categoria', 'titolo', 'short_trama', 'stato', 'premium', 'n_serie', 'tot_episodi')
    list_editable = ('titolo', 'stato', 'premium', 'n_serie', 'tot_episodi')


@admin.register(Animegenere)
class AnimeGenereAdmin(admin.ModelAdmin):
    list_display = ('id', 'id_anime', 'id_genere')
    list_editable = ('id_anime', 'id_genere')


@admin.register(Genere)
class GenereAdmin(admin.ModelAdmin):
    list_display = ('descrizione',)


@admin.register(Serie)
class SerieAdmin(admin.ModelAdmin):
    list_display = ('id', 'numero', 'id_anime', 'nome', 'id_stag_anno', 'n_episodi')
    list_editable = ('id_anime', 'nome', 'id_stag_anno', 'n_episodi')


@admin.register(Stagionedellanno)
class StagionedellannoAdmin(admin.ModelAdmin):
    list_display = ('id', 'stagione', 'anno')
    list_editable = ('stagione', 'anno')
    ordering = ('anno', 'stagione')


@admin.register(Episodio)
class EpisodioAdmin(admin.ModelAdmin):
    list_display = ('id', 'numero', 'titolo', 'id_anime', 'id_serie', 'data_car', 'ora_car')
    list_editable = ('numero', 'titolo', 'id_anime', 'id_serie', 'data_car', 'ora_car')
    ordering = ('id',)


@admin.register(AccountPremium)
class AccountPremiumAdmin(admin.ModelAdmin):
    list_display = ('id', 'id_account', 'id_premium', 'scaduto')
