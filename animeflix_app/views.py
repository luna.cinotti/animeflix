import string
from enum import Enum, auto
# Create your views here.
from django.db.models import Q
from django.http import JsonResponse
from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from .forms import ReviewForm
from .models import Anime, Premium, AccountPremium, Utente, Lista, ListaIncludeAnime, Evaluation
from django.views.generic import TemplateView, DetailView, ListView


class LoadMoreButtonName(Enum):
    _order_ = 'genre popular anime_premium'
    genre = auto()
    popular = auto()
    anime_premium = auto()


class HomePageView(TemplateView):
    template_name = "animeflix_app/pages/home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['allAnime'] = Anime.objects.all()
        return context


class AnimeDetailView(DetailView):
    template_name = "animeflix_app/pages/animeDetail.html"
    model = Anime
    object = None

    def get_object(self, queryset=None):
        return get_object_or_404(Anime, pk=self.request.GET['watch'])

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super().get_context_data()

        context['allAnime'] = Anime.objects.all()
        context['evaluations'] = Evaluation.objects.filter(anime=self.object.pk).all()
        context['stars_list'] = [i for i in range(1, 6)]

        context['reviews_tab'] = False
        if 'reviews' in self.request.path:
            context['reviews_tab'] = True
            context['user_premium'] = False

            user = None
            if self.request.user.is_authenticated:
                user = Utente.objects.filter(account=self.request.user).first()

            if user and user.abbonato:
                context['user_premium'] = True

        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()

        idanime = self.request.session.get('idanime', False)
        if idanime:
            del (self.request.session['idanime'])
        else:
            idanime = self.object.pk

        single_anime = Anime.objects.filter(id=idanime).first()
        single_anime_series = single_anime.get_series()
        series_eps = []
        for s in single_anime_series:
            series_eps.append((s, s.get_episodes()))

        context['single_anime'] = single_anime
        context['series_eps'] = series_eps

        return render(request, self.template_name, context=context)

    def post(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            user = Utente.objects.filter(account=self.request.user).first()
            acc_premium = AccountPremium.objects.select_related('id_account').filter(id_account=user).first()

            if 'addanime' in self.request.POST:
                anime = Anime.objects.filter(id=self.request.POST['addanime']).first()
                fav_list = user.get_account_lists().first()

                if anime.premium and (not acc_premium or acc_premium.scaduto):
                    messages.error(request, "This anime is premium only!")
                elif ListaIncludeAnime.objects.filter(id_account=user, id_anime=anime, nome_lista=fav_list).exists():
                    messages.error(request, "Anime already added!")
                else:
                    list_inc_anime = ListaIncludeAnime(id_account=user, id_anime=anime, nome_lista=fav_list)
                    list_inc_anime.save()
                    messages.success(request, "Anime added successfully!")

                self.request.session['idanime'] = self.request.POST['addanime']

            elif 'watchingAnime' in self.request.POST:
                anime = Anime.objects.filter(id=self.request.POST['watchingAnime']).first()
                fav_list = user.get_account_lists().last()

                if anime.premium and (not acc_premium or acc_premium.scaduto):
                    messages.error(request, "This anime is premium only!")
                elif not ListaIncludeAnime.objects.filter(id_account=user, id_anime=anime,
                                                          nome_lista=fav_list).exists():
                    list_inc_anime = ListaIncludeAnime(id_account=user, id_anime=anime, nome_lista=fav_list)
                    list_inc_anime.save()

                if not anime.premium or (anime.premium and acc_premium and not acc_premium.scaduto):
                    messages.success(request, f"You are watching {anime.titolo}!")

                self.request.session['idanime'] = self.request.POST['watchingAnime']
        else:
            messages.error(request, "You must be logged in!")
            self.request.session['idanime'] = self.get_object().id

        return redirect(self.request.path + '?watch=' + str(self.request.session['idanime']))


class NewAnimeReview(AnimeDetailView, CreateView):
    template_name = 'animeflix_app/animeEvaluation/newReview.html'
    form_class = ReviewForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # If invalid form is passed in kwargs then update it in context
        if 'form' in kwargs:
            context['form'] = kwargs['form']

        return context

    def post(self, request, *args, **kwargs):
        review_form = self.get_form(self.form_class)

        if not review_form.is_valid():
            return self.form_invalid(review_form)

        anime = self.get_object()
        anime_eval = review_form.save(commit=False)
        anime_eval.anime = anime
        anime_eval.author = Utente.objects.filter(account=self.request.user).first()
        anime_eval.title = review_form.data['title']
        anime_eval.review = review_form.data['review']
        anime_eval.save()

        return redirect(reverse('v_anime_reviews') + '?watch=' + str(anime.pk))

    def form_invalid(self, form):
        for field in form.errors:
            form[field].field.widget.attrs['class'] += " is-invalid"

        return self.render_to_response(self.get_context_data(form=form))


class UpdateAnimeReview(AnimeDetailView, UpdateView):
    template_name = 'animeflix_app/animeEvaluation/newReview.html'
    form_class = ReviewForm

    def get_form_kwargs(self):
        """Return the keyword arguments for instantiating the form."""
        kwargs = super().get_form_kwargs()
        kwargs.update({'instance': Evaluation.objects.get(pk=self.kwargs['id_review'])})

        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # If invalid form is passed in kwargs then update it in context
        if 'form' in kwargs:
            context['form'] = kwargs['form']

        return context

    def post(self, request, *args, **kwargs):
        review_form = self.get_form(self.form_class)

        if not review_form.is_valid():
            return self.form_invalid(review_form)

        anime_eval = review_form.instance
        anime_eval.author = Utente.objects.filter(account=self.request.user).first()
        anime_eval.title = review_form.data['title']
        anime_eval.review = review_form.data['review']
        anime_eval.save(update_fields=['vote', 'title', 'review'])

        return redirect(reverse('v_anime_reviews') + '?watch=' + str(anime_eval.anime.pk))

    def form_invalid(self, form):
        for field in form.errors:
            form[field].field.widget.attrs['class'] += " is-invalid"

        return self.render_to_response(self.get_context_data(form=form))


class DeleteAnimeReview(DeleteView):
    template_name = 'animeflix_app/animeEvaluation/deleteReviewModal.html'
    model = Evaluation
    success_message = 'Review deleted successfully!'

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)

        # Set success url
        id_anime = request.POST['watch']
        self.success_url = reverse_lazy('v_anime_reviews') + '?watch=' + id_anime

        return super().delete(request, *args, **kwargs)


class LoadMoreDataView(TemplateView):
    def get(self, request, *args, **kwargs):
        end = int(request.GET['num_cards'])
        start = end - (int(request.GET['offset']))
        page = request.GET['loadMoreAnime']
        more_anime = []
        is_maxed = False

        if page == LoadMoreButtonName.genre.name:
            selected_genres = self.request.GET.getlist('genres')
            genre_anime = Anime.objects.values().order_by('titolo')
            if selected_genres != [] and selected_genres[0] != 'All':
                genre_anime = Anime.objects.values().filter(
                    animegenere__id_genere__descrizione__in=selected_genres).distinct()
            more_anime = genre_anime[start:end]
            is_maxed = True if start > len(genre_anime) else False

        elif page == LoadMoreButtonName.anime_premium.name:
            premium_anime = Anime.objects.values().order_by('titolo').filter(premium=True)
            more_anime = premium_anime[start:end]
            is_maxed = True if start > len(premium_anime) else False
        else:
            print('Houston, we have a problem in loadMoreDataView\n', page)

        return JsonResponse(
            {'moreAnime': list(more_anime), 'isMaxed': is_maxed}, safe=False)


class VideoPageView(TemplateView):
    template_name = "animeflix_app/videoSections/updated.html"
    show_items_limit = 2

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if 'updated' in self.request.path:
            context['inUpdated'] = True

        context['allAnime'] = Anime.objects.all()
        context['anime'] = context['allAnime']
        context['anime_limit'] = self.show_items_limit

        return context


class VideoPageViewAlphabetical(VideoPageView):
    template_name = "animeflix_app/videoSections/alphabetical.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        alphabet = list(string.ascii_uppercase)
        alphabet.insert(0, 'All')
        alphabet.append('#')
        context['alphabet'] = alphabet

        letter = self.request.GET.get('group', 'A')
        if letter and letter != 'All' and letter != '#':
            context['anime'] = context['allAnime'].filter(titolo__istartswith=letter).all()
        elif letter == '#':
            context['anime'] = context['allAnime'].filter(titolo__iregex='^[0-9]').all()

        return context


class VideoPageViewSimulcast(VideoPageView):
    template_name = "animeflix_app/pages/video.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['anime'] = Anime.objects.filter(stato=False)
        return context


class VideoPageViewPopular(VideoPageView):
    template_name = "animeflix_app/videoSections/popular.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['inPopular'] = True
        context['stars_list'] = [i for i in range(1, 6)]
        # context['loadMoreName'] = LoadMoreButtonName.popular.name

        order = self.request.GET.get('order', 'high')
        tot = self.show_items_limit+5
        context['anime'] = context['allAnime'].order_by('-avg_rating', 'titolo')[:tot]
        if order == 'low':
            context['anime'] = context['allAnime'].order_by('avg_rating', 'titolo')[:tot]

        return context


class VideoPageViewCategory(VideoPageView):
    template_name = "animeflix_app/videoSections/category.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['categories'] = Anime.objects.order_by('categoria').values_list('categoria', flat=True).distinct()
        category = self.request.GET.get('search', 'All')
        if category != 'All':
            context['anime'] = context['allAnime'].filter(categoria=category)
        return context


class VideoPageViewGenre(VideoPageView):
    template_name = "animeflix_app/videoSections/genre.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['loadMoreName'] = LoadMoreButtonName.genre.name

        selected_genres = self.request.GET.getlist('genres', ['All'])
        context['selected_genres'] = selected_genres
        context['genres'] = Anime.objects.order_by('animegenere__id_genere__descrizione'). \
            values_list('animegenere__id_genere__descrizione', flat=True).distinct()

        if selected_genres != [] and selected_genres[0] != 'All':
            context['anime'] = context['allAnime'].filter(
                animegenere__id_genere__descrizione__in=selected_genres).distinct()[:self.show_items_limit]
        else:
            context['anime'] = context['anime'][:self.show_items_limit]
        return context


class VideoPageViewSeason(VideoPageView):
    template_name = "animeflix_app/videoSections/season.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['seasons'] = Anime.objects.order_by('serie__id_stag_anno__anno'). \
            values_list('serie__id_stag_anno__stagione', 'serie__id_stag_anno__anno').distinct()
        season = self.request.GET.get('season', 'All').split('_')[0]
        year = int(self.request.GET['season'].split('_')[1]) if season != 'All' else 0

        if season != 'All':
            context['anime'] = context['allAnime'].order_by('serie__id_stag_anno__anno').filter(
                Q(serie__id_stag_anno__stagione=season) & Q(serie__id_stag_anno__anno=year)).distinct()
        return context


class VideoPageViewPremium(VideoPageView):
    template_name = "animeflix_app/videoSections/animePremium.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['loadMoreName'] = LoadMoreButtonName.anime_premium.name
        context['anime'] = context['allAnime'].filter(premium=True)[:self.show_items_limit]
        return context


class PremiumPageView(TemplateView):
    template_name = "animeflix_app/pages/premium.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['allAnime'] = Anime.objects.all()
        context['premiumOfferts'] = Premium.objects.all()
        context['existing_premium'] = None

        if self.request.user.is_authenticated:
            user = Utente.objects.filter(account=self.request.user).first()
            pr_account = AccountPremium.objects.select_related('id_premium').filter(id_account=user).first()
            if pr_account is not None:
                context['existing_premium'] = pr_account

        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)

        # If user is not logged, then redirect to login page
        if not request.user.is_authenticated:
            return redirect('login', *args, **kwargs)

        account_premium_id = request.POST.get('delete-subscription', None)

        if account_premium_id and context['existing_premium']:
            account_premium = context['existing_premium']
            account_premium.delete()
            messages.success(request, 'Unsubscribe successfully!')
            return redirect('profile')

        offert_id = request.POST.get('offert', None)

        if offert_id:
            offert_id = int(offert_id)
            context['offert_id'] = offert_id
            context['all_offerts'] = Premium.objects.all()

            user = Utente.objects.filter(account=request.user).first()
            premium = Premium.objects.filter(id=offert_id).first()

            # Check if user has already a premium account
            pr_account = context['existing_premium']

            if pr_account and pr_account.scaduto:
                # Update premium account
                pr_account.id_premium = premium
                pr_account.scaduto = False
                pr_account.save(update_fields=['scaduto'])
            else:
                # If user has an other premium account then delete it
                if pr_account and pr_account.id_premium != offert_id:
                    pr_account.delete()

                # Create new premium account
                pr_account = AccountPremium(id_account=user, id_premium=premium)
                pr_account.save()

            # Update user
            user.abbonato = True
            user.save(update_fields=['abbonato'])

            return redirect('subscribe')

        return render(request, self.template_name, context)


class ChangeAccountToPremium(TemplateView):
    template_name = "animeflix_app/pages/subscribe.html"


class MyAnimeListView(ListView):
    template_name = "animeflix_app/pages/myAnimeList.html"
    model = Lista

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['allAnime'] = Anime.objects.all()
        context['utente'] = Utente.objects.filter(account=self.request.user).first()
        return context

    def post(self, request, *args, **kwargs):
        ListaIncludeAnime.objects.filter(id=self.request.POST['deleteAnime']).delete()
        return redirect(self.request.path)
