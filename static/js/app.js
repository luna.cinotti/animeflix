// jQuery Functions
$(document).ready(function () {

    $('.text-editor-btn').click(function() {
        // TinyMCE configuration
        tinymce.init({
            selector: 'textarea',
            plugins: 'autoresize advlist link image lists',
            browser_spellcheck: true,
            forced_root_block : '',
            branding: false,
        });
    });

    // Get current url page
    let currentUrl = $(location).attr('pathname');

    function activeByUrl(elem) {
        if ($(elem).attr('href') === currentUrl)
            $(elem).addClass('active');
    }

    // Function that manages style of header's nav-link by current url
    $('header .navbar-nav a.nav-link').each(function () {
        activeByUrl(this);
    });

    // Function that manages style of Video's nav-tabs by current url
    $('ul.nav-tabs li.nav-item a.nav-link').each(function () {
        $('header .navbar-nav a.nav-link:contains(Video)').addClass('active');
        activeByUrl(this);
    });

    if (currentUrl.includes('forum'))
        $('header .navbar-nav a.nav-link:contains(Forum)').addClass('active');

    // Function to manages multiple events on the searchbar
    $('#search-anime').click(function () {
        $('header button.search-box img').addClass('primary-img-filter');
        $(this).data.clicked = true;
    }).mouseover(function () {
        $('header button.search-box img').addClass('primary-img-filter');
    }).mouseout(function () {
        if (!$('#search-anime').data.clicked)
            $('header button.search-box img').removeClass('primary-img-filter');
    });

    // Function to detect a click outside of the searchbar
    $(document).on('click', function (event) {
        if (!$(event.target).closest('#search-anime').length) {
            $('header button.search-box img').removeClass('primary-img-filter')
            $('#search-anime').data.clicked = false;
        }
    });

});
