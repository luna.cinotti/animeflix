from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.contrib.auth.views import LogoutView

from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy
from django.core.cache import cache
from django.views.generic import FormView, DetailView
from django.views.generic.edit import UpdateView, DeleteView

from animeflix_app.models import Utente, Lista, AccountPremium
from auth_app.forms import LoginForm, RegistrationForm, LISTS, ProfileForm, AccountForm

# Create your views here.
from forum.models import Author


class Logout(LogoutView):
    next_page = reverse_lazy('home')

    def dispatch(self, request, *args, **kwargs):
        current_user = request.user

        # Delete his cache record if exists
        if cache.get('seen_%s' % current_user.username):
            cache.delete('seen_%s' % current_user.username)

        return super().dispatch(request, *args, **kwargs)


class LoginFormView(FormView):
    template_name = 'auth_app/login.html'
    success_url = reverse_lazy('home')
    form_class = LoginForm

    def form_invalid(self, form):
        for field in form.errors:
            form[field].field.widget.attrs['class'] += " is-invalid"

        return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, form):
        # User authentication
        user_by_email = User.objects.filter(email__exact=form.data["account"]).first()
        user_by_name = User.objects.filter(username__exact=form.data["account"]).first()

        if user_by_email is None and user_by_name is None:
            # account doesn't exist
            form.add_error("account", "Email or username not found")
            return self.form_invalid(form)

        user = None

        if user_by_name:
            if not user_by_name.is_active:
                # account disabled
                form.add_error("account", "Account disactivated")
                return self.form_invalid(form)
            user = authenticate(username=form.data["account"], password=form.cleaned_data["password"])

        if user_by_email and user_by_email.check_password(form.cleaned_data["password"]):
            user = user_by_email

        if user is None:
            # invalid password
            form.add_error("password", "Invalid password")
            return self.form_invalid(form)

        elif not user.is_active:
            # account disabled
            form.add_error("account", "Account disactivated")
            return self.form_invalid(form)

        # user login
        login(self.request, user)

        # redirect a success_url
        return super().form_valid(form)


class ResetPasswordView(FormView):
    template_name = 'auth_app/changePassword.html'
    form_class = LoginForm
    success_url = reverse_lazy('login')
    success_message = 'Password changed successfully!'

    def form_invalid(self, form):
        for field in form.errors:
            form[field].field.widget.attrs['class'] += " is-invalid"

        return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, form):
        # Check if user exists
        user_by_email = User.objects.filter(email__exact=form.data["account"]).first()

        if user_by_email is None:
            # invalid account
            form.add_error("account", "Email address not found")
            return self.form_invalid(form)

        if user_by_email.check_password(form.cleaned_data["password"]):
            print(user_by_email.password, form.cleaned_data["password"])
            # invalid password
            form.add_error("password", "Password is identical to the old one")
            return self.form_invalid(form)

        user_by_email.set_password(form.cleaned_data["password"])
        user_by_email.save()

        messages.success(self.request, self.success_message)

        # redirect a success_url
        return super().form_valid(form)


class RegistrationFormView(FormView):
    template_name = 'auth_app/registration.html'
    success_url = reverse_lazy('premium')
    form_class = RegistrationForm

    def form_invalid(self, form):
        for field in form.errors:
            form[field].field.widget.attrs['class'] += " is-invalid"

        return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, form):
        if User.objects.filter(email=form.data["email"]).exists():
            form.add_error("email", "An account already exists with this email")
            return self.form_invalid(form)

        if User.objects.filter(username=form.data["username"]).exists():
            form.add_error("username", "This username is already used")
            return self.form_invalid(form)

        account = User()
        account.email = form.data["email"]
        account.username = form.data["username"]
        account.password = make_password(form.data["password"])
        account.save()

        user = Utente(account=account)

        age = form.data["age"]
        try:
            age = int(age)
        except ValueError:
            age = None

        user.eta = age
        user.nazionalita = form.data["country"]
        user.save()

        # Create account lists
        for list_name in LISTS:
            account_list = Lista(nome=list_name, id_account=Utente.objects.filter(account=user.account).first())
            account_list.save()

        # redirect a success_url
        return super().form_valid(form)


class ProfileView(DetailView):
    template_name = 'auth_app/profile.html'
    model = Utente

    def get_object(self, queryset=None):
        return get_object_or_404(Utente.objects.select_related('account'), pk=self.request.user.pk)


class EditAccountView(UpdateView):
    template_name = 'auth_app/editAccount.html'
    success_url = reverse_lazy('profile')
    form_class = ProfileForm
    second_form_class = AccountForm
    object = None

    def get_object(self, queryset=None):
        return get_object_or_404(Utente.objects.select_related('account'), pk=self.kwargs['pk'])

    # Send both forms to the context
    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        if 'account_form' not in kwargs:
            kwargs['account_form'] = AccountForm(instance=self.object.account)

        if 'user_form' not in kwargs:
            kwargs['user_form'] = self.get_form(self.form_class)

        return super().get_context_data(**kwargs)

    def form_invalid(self, account_form):
        for field in account_form.errors:
            account_form[field].field.widget.attrs['class'] += " is-invalid"

        return self.render_to_response(self.get_context_data(account_form=account_form))

    def post(self, *args, **kwargs):
        self.object = self.get_object()
        account_form = self.get_form(self.second_form_class)
        user_form = ProfileForm(self.request.POST, self.request.FILES)

        if not account_form.is_valid():
            return self.form_invalid(account_form)

        # Update account data
        account = User.objects.get(pk=self.object.account.pk)
        account.email = account_form.data['email']
        account.username = account_form.data['username']

        if account.password != account_form.data['password']:
            account.set_password(account_form.data["password"])
            login(self.request, account)
        account.save(update_fields=['email', 'username', 'password'])

        # Update user data
        self.object = user_form.instance
        self.object.account = account

        upload_folder = self.object.avatar.field.upload_to
        image_name = upload_folder + '-user' + str(self.object.pk) + '.png'
        image_path = upload_folder + '/' + image_name

        # Check if image file already exists
        if self.object.avatar.storage.exists(image_path):
            input = self.request.POST.get('image_file', None)

            # If user avatar has been changed in default image or another
            if self.request.FILES or input and 'default' in input:
                # Delete image file from storage and set default image
                self.object.avatar.storage.delete(image_path)

                self.object.avatar = self.object.get_default_avatar_path()
                self.object.save(update_fields=['avatar'])

        if self.request.FILES:
            # Save new image file
            self.object.avatar.save(image_name, self.request.FILES['avatar'], save=False)
            self.object.save(update_fields=['avatar'])

        age = user_form.data['eta']
        try:
            age = int(age)
        except ValueError:
            age = None

        self.object.eta = age
        self.object.nazionalita = user_form.data['nazionalita']
        self.object.save(update_fields=['eta', 'nazionalita'])

        return redirect(self.success_url)


class DeleteAccountView(DeleteView):
    template_name = 'auth_app/deleteAccountModal.html'
    model = Utente
    success_url = reverse_lazy('home')
    success_message = 'Account deleted successfully!'

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)

        user = self.get_object()
        is_author = Author.objects.filter(user=user).exists()

        if is_author:
            if user.author.n_thread != 0 or user.author.n_post != 0:
                # if user has a premium subscription, then delete it
                if user.abbonato:
                    premium = AccountPremium.objects.get(id_account=user)
                    if premium:
                        premium.delete()

                # Delete user avatar and set default if it is different to default
                if 'default' not in user.avatar.name:
                    user.avatar.storage.delete(user.avatar.name)
                    user.avatar = user.get_default_avatar_path()
                    user.save(update_fields=['avatar'])

                # Logout and account deactivation
                logout(request)
                user.account.is_active = False
                user.account.save(update_fields=['is_active'])

                return redirect(self.success_url)

        return super().delete(request, *args, **kwargs)
