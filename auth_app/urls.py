from django.urls import path

from auth_app.views import LoginFormView, RegistrationFormView, EditAccountView, ProfileView, \
    Logout, DeleteAccountView, ResetPasswordView

urlpatterns = [
    path('login', LoginFormView.as_view(), name='login'),
    path('logout', Logout.as_view(), name='logout'),
    path('set-password', ResetPasswordView.as_view(), name='reset_pwd'),
    path('registration', RegistrationFormView.as_view(), name='registration'),
    path('profile', ProfileView.as_view(), name='profile'),
    path('edit-profile<pk>', EditAccountView.as_view(), name='edit_account'),
    path('delete-account<pk>', DeleteAccountView.as_view(), name='delete_account')
]
