from django import forms
from django.contrib.auth.models import User

from animeflix_app.models import Utente

LISTS = ['MyList', 'Seen']


class LoginForm(forms.Form):
    account = forms.CharField(
        error_messages={
            'invalid': 'Please use a valid email or username'
        },
        required=True,
        label="Email or Username*",
        widget=forms.TextInput(attrs={"class": "form-control form-input"})
    )

    password = forms.CharField(
        required=True,
        label="Password*",
        widget=forms.PasswordInput(attrs={"class": "form-control form-input"})
    )

    def clean_password(self):
        password = self.cleaned_data['password']
        if len(password) < 6:
            raise forms.ValidationError('Password must be at least 6 characters')

        if ' ' in password:
            raise forms.ValidationError('Password should not contain any space')
        return password


class RegistrationForm(forms.Form):
    username = forms.CharField(
        error_messages={
            'invalid': 'Please use a valid username'
        },
        required=True,
        label="Username*",
        widget=forms.TextInput(attrs={"class": "form-control form-input"})
    )
    email = forms.EmailField(
        error_messages={
            'invalid': 'Please use a valid email'
        },
        required=True,
        label="Email Address*",
        widget=forms.EmailInput(attrs={"class": "form-control form-input"})
    )

    password = forms.CharField(
        help_text='Passwords must be at least 6 characters, but no spaces',
        required=True,
        label="Password*",
        widget=forms.PasswordInput(attrs={"class": "form-control form-input"})
    )

    age = forms.IntegerField(
        required=False,
        min_value=0,
        widget=forms.NumberInput(attrs={"class": "form-control form-input"})
    )

    country = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={"class": "form-control form-input"})
    )

    def clean_password(self):
        password = self.cleaned_data['password']
        if len(password) < 6:
            raise forms.ValidationError('Password is too short')
        if ' ' in password:
            raise forms.ValidationError('Password should not contain any space')
        return password

    def clean_age(self):
        age = self.cleaned_data['age']
        if age == 0:
            raise forms.ValidationError('Please insert a valid age or leave blank')
        return age


class ProfileForm(forms.ModelForm):
    avatar = forms.ImageField(
        required=False,
        widget=forms.FileInput()
    )

    eta = forms.IntegerField(
        required=False,
        min_value=0,
        label="Age",
        widget=forms.TextInput(attrs={"class": "form-control form-input"})
    )

    nazionalita = forms.CharField(
        required=False,
        label="Country",
        widget=forms.TextInput(attrs={"class": "form-control form-input"})
    )

    class Meta:
        model = Utente
        fields = ['avatar', 'eta', 'nazionalita']


class AccountForm(forms.ModelForm):
    username = forms.CharField(
        error_messages={
            'invalid': 'Please use a valid username'
        },
        required=True,
        label="Username*",
        widget=forms.TextInput(attrs={"class": "form-control form-input"})
    )
    email = forms.EmailField(
        error_messages={
            'invalid': 'Please use a valid email'
        },
        required=True,
        label="Email Address*",
        widget=forms.EmailInput(attrs={"class": "form-control form-input"})
    )

    password = forms.CharField(
        required=True,
        label="Password*",
        widget=forms.PasswordInput(attrs={"class": "form-control form-input"}, render_value=True)
    )

    def clean_password(self):
        password = self.cleaned_data['password']
        if len(password) < 6:
            raise forms.ValidationError('Password must be at least 6 characters')

        if ' ' in password:
            raise forms.ValidationError('Password should not contain any space')
        return password

    class Meta:
        model = User
        fields = ['email', 'username', 'password']
