import datetime
from django.core.cache import cache
from django.conf import settings

from animeflix_app.models import AccountPremium, Utente


class ActiveUserMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        current_user = request.user

        if request.user.is_authenticated:
            now = datetime.datetime.now()
            cache.set('seen_%s' % current_user.username, now, settings.USER_LASTSEEN_TIMEOUT)

            # If user has a premium account then check if it is expired
            premium_account = AccountPremium.objects.filter(id_account__account=current_user).first()
            if premium_account:
                expired_date = datetime.datetime(premium_account.data_fine.year, premium_account.data_fine.month,
                                                 premium_account.data_fine.day)
                if expired_date < now:
                    premium_account.scaduto = True
                    premium_account.save(update_fields=['scaduto'])
                    utente = Utente.objects.get(account=current_user)
                    utente.abbonato = False
                    utente.save(update_fields=['abbonato'])

        return response
