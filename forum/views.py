import datetime
import math

from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Prefetch, Max
from django import forms
from django.forms import TextInput
from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy, reverse
from django.views.generic import DetailView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import MultipleObjectMixin

from animeflix_app.models import Utente, Anime
from forum.forms import ThreadForm, PostForm
from forum.models import Thread, Post, Author, Category


class CustomLoginRequiredMixin(LoginRequiredMixin):
    """ The LoginRequiredMixin extended to add a relevant message to the
    messages framework by setting the ``permission_denied_message``
    attribute. """

    redirect_field_name = 'redirect_from'
    login_url = reverse_lazy('login')
    permission_denied_message = 'You have to be logged in to access that page'

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            messages.add_message(request, messages.ERROR,
                                 self.permission_denied_message)
            return self.handle_no_permission()

        return super().dispatch(request, *args, **kwargs)


class ForumPageView(ListView):
    template_name = "forum/forum.html"
    model = Thread
    context_object_name = 'threads'
    queryset = Thread.objects.all()
    paginate_by = 0
    object_list = None

    def get_context_data(self, **kwargs):
        self.object_list = self.get_queryset()
        if self.object_list is not None:
            self.paginate_by = math.ceil(self.object_list.count() * 0.4)
        context = super().get_context_data(**kwargs)

        context['allAnime'] = Anime.objects.all()
        context['categories'] = Category.objects.all()
        context['threads_titles'] = Thread.objects.order_by('title').values('id', 'title').all()
        context['search_title'] = self.request.GET.get('th-title', None)
        context['search_category'] = self.request.GET.get('th-category', None)

        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data()

        thread_id = self.request.GET.get('th-id', None)
        if thread_id:
            return HttpResponseRedirect(reverse('thread_detail', kwargs={'pk': int(thread_id)}))

        return render(request, self.template_name, context=context)

    def get_queryset(self):
        queryset = Thread.objects.select_related('creator__user__account', 'category') \
            .prefetch_related(Prefetch('post_set', queryset=Post.objects.select_related('author__user__account'))) \
            .annotate(max_mod_date=Max('post__mod_date')).order_by('-max_mod_date').all()

        title_searched = self.request.GET.get('th-title', None)
        thread_id = self.request.GET.get('th-id', None)

        if title_searched and not thread_id:
            queryset = queryset.filter(title__icontains=title_searched)
            if not queryset.exists():
                return None

        thread_category = self.request.GET.get('th-category', None)
        if thread_category:
            queryset = queryset.filter(category__description__exact=thread_category)

        return queryset


class NewThreadFormView(CustomLoginRequiredMixin, CreateView):
    template_name = "forum/newThread.html"
    form_class = ThreadForm
    second_form_class = PostForm
    object = None

    # Send both forms to the context
    def get_context_data(self, **kwargs):
        """Insert the form into the context dict."""
        if 'thread_form' not in kwargs:
            kwargs['thread_form'] = self.get_form(self.form_class)

        if 'post_form' not in kwargs:
            kwargs['post_form'] = self.get_form(self.second_form_class)

        context = super().get_context_data(**kwargs)
        context['allAnime'] = Anime.objects.all()

        return context

    def get_success_url(self):
        return reverse_lazy('thread_detail', kwargs={'pk': self.object.pk})

    def forms_invalid(self, thread_form, post_form):
        for field in thread_form.errors:
            thread_form[field].field.widget.attrs['class'] += " is-invalid"

        for field in post_form.errors:
            post_form[field].field.widget.attrs['class'] += " is-invalid"

        return self.render_to_response(self.get_context_data(thread_form=thread_form, post_form=post_form))

    def post(self, request, *args, **kwargs):
        thread_form = self.get_form(self.form_class)
        post_form = self.get_form(self.second_form_class)

        if not thread_form.is_valid() or not post_form.is_valid():
            return self.forms_invalid(thread_form, post_form)

        thread_category = Category.objects.filter(description__exact=thread_form.data['category']).first()
        thread_title = thread_form.data['title']
        post_message = post_form.data['message']

        self.object = thread_form.save(commit=False)

        # Create new author if not exists
        user = Utente.objects.select_related('account').filter(account=self.request.user).first()
        author = Author.objects.select_related('user__account').get_or_create(user=user)[0]

        # Create new thread
        self.object.creator = author
        self.object.category = thread_category
        self.object.title = thread_title
        self.object.save()

        # Create post of thread
        post = Post(id_thread=self.object, author=self.object.creator, message=post_message, is_thread_post=True)
        post.save()

        return HttpResponseRedirect(self.get_success_url())


class ThreadDetailView(DetailView, MultipleObjectMixin):
    template_name = "forum/threadDetail.html"
    model = Thread
    object = None
    object_list = None

    def get_context_data(self, **kwargs):
        self.object = Thread.objects.select_related('creator__user__account') \
            .prefetch_related('category',
                              Prefetch('post_set', queryset=Post.objects.select_related(
                                  'author__user__account'))).get(id=self.object.pk)
        self.object_list = self.object.post_set.prefetch_related(
            Prefetch('quote', queryset=Post.objects.select_related('author__user__account'))).all()

        context = super().get_context_data()
        context['allAnime'] = Anime.objects.all()

        return context

    def get_paginate_by(self, queryset):
        queryset = self.get_queryset()
        return math.ceil(queryset.count() * 0.15)


class EditThreadTitle(ThreadDetailView, UpdateView):
    template_name = "forum/updates/updateThreadTitle.html"
    form_class = forms.modelform_factory(Thread, fields=['title'],
                                         widgets={'title': TextInput(attrs={"class": "form-control form-input"})})

    def get_success_url(self):
        page = self.request.POST.get('page', None)
        page_url = '?page=' + page if page else ''

        return self.object.get_absolute_url() + page_url

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['allAnime'] = Anime.objects.all()
        context['edit_title'] = self.object.pk

        return context

    def form_invalid(self, form):
        for field in form.errors:
            form[field].field.widget.attrs['class'] += " is-invalid"

        return self.render_to_response(self.get_context_data(form=form))


class DeleteThreadView(DeleteView):
    template_name = 'forum/modals/deleteThreadModal.html'
    model = Thread
    success_url = reverse_lazy('forum')
    success_message = 'Thread deleted successfully!'

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super().delete(request, *args, **kwargs)


class NewPostFormView(ThreadDetailView, CreateView):
    template_name = "forum/newPost.html"
    model = Thread
    form_class = PostForm

    def get_success_url(self):
        page_url = '?page=' + self.request.POST.get('page', '')
        return self.object.get_absolute_url() + page_url

    def get_context_data(self, **kwargs):
        self.object = self.get_object()
        context = super().get_context_data(**kwargs)
        context['allAnime'] = Anime.objects.all()

        # If invalid form is passed in kwargs then update it in context
        if 'form' in kwargs:
            context['form'] = kwargs['form']

        if 'id_quote' in self.kwargs:
            context['quote'] = Post.objects.select_related('author__user__account').get(id=self.kwargs['id_quote'])

        return context

    def form_invalid(self, form):
        for field in form.errors:
            form[field].field.widget.attrs['class'] += " is-invalid"

        return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, form):
        self.object = self.get_object()

        # Create new author if not exists
        user = Utente.objects.filter(account=self.request.user).first()
        author = Author.objects.get_or_create(user=user)[0]

        # Create new post
        post = form.save(commit=False)
        post.id_thread = self.object
        post.author = author
        post.message = form.data['message']

        if 'id_quote' in self.kwargs:
            post.quote = Post.objects.get(pk=self.kwargs['id_quote'])
        post.save()

        return HttpResponseRedirect(self.get_success_url())


class EditPostFormView(ThreadDetailView, UpdateView):
    template_name = 'forum/updates/updatePost.html'
    form_class = PostForm

    def get_form_kwargs(self):
        """Return the keyword arguments for instantiating the form."""
        kwargs = super().get_form_kwargs()
        kwargs.update({'instance': Post.objects.get(pk=self.kwargs['id_post'])})

        return kwargs

    def get_success_url(self):
        page_url = '?page=' + self.request.GET.get('page', '')
        return self.object.get_absolute_url() + page_url

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['allAnime'] = Anime.objects.all()

        # If invalid form is passed in kwargs then update it in context
        if 'form' in kwargs:
            context['form'] = kwargs['form']

        context['edit_post'] = self.kwargs['id_post']

        return context

    def form_invalid(self, form):
        for field in form.errors:
            form[field].field.widget.attrs['class'] += " is-invalid"

        return self.render_to_response(self.get_context_data(form=form))

    def form_valid(self, form):
        # Update post
        post = form.instance
        post.message = form.data['message']
        post.mod_date = datetime.datetime.today()
        post.save(update_fields=['message', 'mod_date'])

        return HttpResponseRedirect(self.get_success_url())


class DeletePostView(DeleteView):
    template_name = 'forum/modals/deletePostModal.html'
    model = Post
    success_message = 'Post deleted successfully!'

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)

        # Set success url
        page = request.POST.get('page', '')
        self.success_url = reverse_lazy('thread_detail', kwargs={'pk': kwargs['id_thread']}) + '?page=' + page

        return super().delete(request, *args, **kwargs)
