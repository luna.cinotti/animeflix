from django.contrib.auth.models import User
from forum.models import Category, Thread, Post, Author, Utente
from forum.views import NewThreadFormView
from django.test import RequestFactory, TestCase, Client
from django.urls import reverse


class TestViews(TestCase):

    def setUp(self) -> None:
        self.client = Client()
        self.th_creation_url = reverse('new_thread')

        self.account = User.objects.create(username='pablo', email='pablo80@animeflix.it', password='pablo80pablo80',
                                           is_active=True)
        self.user = Utente.objects.create(account=self.account)
        self.author = Author.objects.create(user=self.user)

        self.th_category = Category.objects.create(description='Anime')
        self.pablo_thread = Thread.objects.create(title='testTitle', category=self.th_category, creator=self.author)
        self.pablo_thread_message = Post.objects.create(message='testMessage', author=self.author,
                                                        id_thread=self.pablo_thread)

    def test_thAnswerNumber(self):
        """testing application function 'get_answers'"""

        self.assertEqual(self.pablo_thread.n_post, 1)
        self.assertEqual(self.pablo_thread.get_answers(), 0)

    def test_threadCreationContext(self):
        """testing view 'NewThreadFormView'"""

        request = RequestFactory().get(self.th_creation_url)
        view = NewThreadFormView()
        view.setup(request)

        self.client.login(username='pablo', password='pablo80pablo80')

        context = view.get_context_data()

        self.assertIn('thread_form', context)
        self.assertIn('post_form', context)


