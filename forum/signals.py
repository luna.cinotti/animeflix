from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from .models import Thread, Post


@receiver([post_save, post_delete], sender=Thread)
def update_creator_counters(sender, instance, **kwargs):
    updated = kwargs.get('update_fields', None)

    if updated is None:
        # Update creator thread and post counter
        creator = instance.creator
        creator.n_thread = creator.thread_set.count()
        creator.n_post = creator.post_set.count()
        creator.save(update_fields=['n_thread', 'n_post'])


@receiver([post_save, post_delete], sender=Post)
def update_post_counters(sender, instance, **kwargs):
    updated = kwargs.get('update_fields', None)

    if not instance.is_thread_post and updated is None:
        # Update author post counter
        author = instance.author
        author.n_post = author.post_set.count()
        author.save(update_fields=['n_post'])

        # Update thread post counter
        thread = instance.id_thread
        thread.n_post = thread.post_set.count()
        thread.save(update_fields=['n_post'])
