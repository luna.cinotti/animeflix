from django.urls import path

from .views import ForumPageView, ThreadDetailView, NewThreadFormView, EditPostFormView, NewPostFormView, \
    EditThreadTitle, DeleteThreadView, DeletePostView

urlpatterns = [
    path('', ForumPageView.as_view(), name='forum'),
    path('new-thread', NewThreadFormView.as_view(), name='new_thread'),
    path('thread<pk>', ThreadDetailView.as_view(), name='thread_detail'),
    path('thread<pk>/edit-title', EditThreadTitle.as_view(), name='edit_title'),
    path('thread<pk>/delete', DeleteThreadView.as_view(), name='delete_thread'),
    path('thread<pk>/new-post', NewPostFormView.as_view(), name='new_post'),
    path('thread<pk>/new-post&reply-to=post<int:id_quote>', NewPostFormView.as_view(), name='reply_post'),
    path('thread<pk>/edit-post<int:id_post>', EditPostFormView.as_view(), name='edit_post'),
    path('thread<id_thread>/delete-post<pk>', DeletePostView.as_view(), name='delete_post')
]
