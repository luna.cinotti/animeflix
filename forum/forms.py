from django import forms
from django.views.generic.edit import UpdateView

from forum.models import Thread, Post, Category


class ThreadForm(forms.ModelForm):
    category = forms.ModelChoiceField(
        queryset=Category.objects.all(),
        error_messages={
            'required': "Don't forget to assign a category"
        },
        required=True,
        label="Category",
        widget=forms.Select(attrs={"class": "form-select", "autofocus": True})
    )

    title = forms.CharField(
        error_messages={
            'required': "Don't forget to assign a title"
        },
        required=True,
        label="Title",
        widget=forms.TextInput(attrs={"class": "form-control form-input"})
    )

    class Meta:
        model = Thread
        fields = ['category', 'title']


class PostForm(forms.ModelForm):
    message = forms.CharField(
        error_messages={
            'required': "Don't forget to write a post"
        },
        required=True,
        label="Post",
        widget=forms.Textarea(attrs={"class": "form-control form-input", "autofocus": True})
    )

    class Meta:
        model = Post
        fields = ['message']
