from django.db import models
from django.urls import reverse

from animeflix_app.models import Utente


# Create your models here.


class Author(models.Model):
    user = models.OneToOneField(Utente, on_delete=models.CASCADE, db_column='user', primary_key=True)
    n_thread = models.PositiveIntegerField(default=0, null=False)
    n_post = models.PositiveIntegerField(default=0, null=False)

    class Meta:
        db_table = 'FORUM_author'

    def __str__(self):
        return str(self.user)

    def is_online(self):
        return self.user.is_online()


class Category(models.Model):
    description = models.CharField(primary_key=True, max_length=50)

    class Meta:
        db_table = 'FORUM_category'
        ordering = ['description']

    def __str__(self):
        return self.description


class Thread(models.Model):
    id = models.BigAutoField(primary_key=True)
    creator = models.ForeignKey(Author, on_delete=models.DO_NOTHING, db_column='creator')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, db_column='category')
    title = models.TextField(blank=False, max_length=100)
    n_post = models.PositiveIntegerField(default=1)

    class Meta:
        db_table = 'FORUM_thread'

    def get_absolute_url(self):
        return reverse('thread_detail', kwargs={'pk': self.id})

    def get_answers(self):
        return self.n_post - 1


class Post(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_thread = models.ForeignKey(Thread, on_delete=models.CASCADE, db_column='id_thread')
    author = models.ForeignKey(Author, on_delete=models.DO_NOTHING, db_column='author')
    quote = models.ForeignKey("Post", on_delete=models.SET_NULL, db_column='quote', null=True)
    message = models.TextField(blank=False)
    is_thread_post = models.BooleanField(default=False, editable=False)
    creation_date = models.DateTimeField(auto_now_add=True, editable=False)
    mod_date = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'FORUM_post'
        ordering = ['creation_date']

    def creation_date_str(self):
        return self.creation_date.strftime("%d-%m-%Y")

    def mod_date_str(self):
        return self.mod_date.strftime("%d-%m-%Y")
