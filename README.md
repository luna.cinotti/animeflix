# AnimeFLIX

## Librerie e Framework usati

* jQuery
* Bootstrap 5
* Django
* TinyMCE
* Django Debug Toolbar

## Installazione

Per usare [jQuery 3.5.1 slim](https://code.jquery.com/jquery/), 
[TinyMCE](https://www.tiny.cloud/docs/quick-start/) e
Bootstrap [Bootstrapt 5](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
li abbiamo linkati come da documentazione.

Per installare [Django Debug Toolbar](https://django-debug-toolbar.readthedocs.io/en/latest/installation.html) abbiamo seguito la sua documentazione

## Passi a seguire per l'installazione e utilizzo di AnimeFlix
1. installare pipenv
2. eseguire i seguenti comandi:
   1. ```pipenv install```
   2. ```pipenv shell```
3. lanciare il processo del server con il commando:
   1. ```python3 manage.py runserver```
4. aprire un browser e collegarsi a localhost:8000

## Come si presenta l'applicazione

#### AUTENTICAZIONE
**Pagina iniziale (homepage):**
![Homepage](screenshots/homepage.PNG)
**Pagina di registrazione:**
![Register](screenshots/register.PNG)
**Pagina di login:**
![Login](screenshots/login.PNG)
**Profilo:**
![Profile](screenshots/profile.PNG)
**Modifica profilo:**
![Edit profile](screenshots/editProfile.PNG)
**Pagina abbonamenti Premium:**
![Premium](screenshots/premium.PNG)

#### ANIME
**Pagina degli anime popolari:**
![Popular anime](screenshots/animeList.PNG)
**Lista degli anime preferiti (richiede l'accesso):**
![User list](screenshots/userList.PNG)
**Elenco video di un anime:**
![Anime video](screenshots/videoList.PNG)
**Alert - visione di un episodio:**
![Watching anime video](screenshots/watchingAnime.PNG)
**Elenco recensioni di un anime (senza accesso):**
![Anime reviews](screenshots/animeReviews(noLogged).PNG)
**Ricerca di un anime:**
![Anime search](screenshots/animeSearch.PNG)

#### FORUM
**Forum - elenco dei thread aperti:**
![forum](screenshots/forum(logged).PNG)
**Ricerca di un thread:**
![Thread search](screenshots/threadSearch.PNG)
**Esempio di thread con delle discussioni:**
![thread1](screenshots/thread1(logged).PNG)
**Esempio di post con citazione:**
![Quote post](screenshots/quote.PNG)
**Risposta ad un post con citazione:**
![Reply with quote](screenshots/replyWithQuote.PNG)
**Aggiunta di un thread (richiede l'accesso):**
![Add thread](screenshots/addThread.PNG)
**Alert - accesso richiesto per rispondere al forum:**
![Login required alert](screenshots/loginRequiredAlert.PNG)
